package tdp.mt.backend.rpa.jar.domain.loginMT;

public class ConvergenciaResponse {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
