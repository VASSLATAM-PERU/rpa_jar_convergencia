package tdp.mt.backend.rpa.jar.main;

import tdp.mt.backend.rpa.jar.domain.Response;
import tdp.mt.backend.rpa.jar.domain.loginMT.ConvergenciaRequest;
import tdp.mt.backend.rpa.jar.domain.loginMT.ConvergenciaResponse;
import tdp.mt.backend.rpa.jar.service.UserService;
import tdp.mt.backend.rpa.jar.util.Constants;
import tdp.mt.backend.rpa.jar.util.Properties;

//@SpringBootApplication
public class RpaMainClass {

    public static void main(String[] args) {

        //SpringApplication.run(RpaMainClass.class, args);
        System.out.println("Rpa Jar started!");

        //capture hostname
        String hostname = UserService.getHostName();

        ConvergenciaRequest cr = new ConvergenciaRequest(Constants.TIPO_DOC, Constants.NUMERO_DE_DOC,
                Constants.COD_ATIS,
                Constants.CANAL_DE_VENTA, Constants.PUNTO_DE_VENTA,
                Constants.ASESOR_NOMBRE,
                Constants.CANAL_EQUIV_CAMPANIA, Constants.ENTIDAD, hostname);

        Response<ConvergenciaResponse> response = UserService.getToken(cr);

        if (response.getResponseCode().equals(Constants.LOGIN_RESPONSE_CODE_OK)) {
            System.out.println("Your encrypted token: " + response.getResponseData().getToken());
            boolean isProduction = System.getenv("JAR_ENVIRONMENT")!=null?System.getenv("JAR_ENVIRONMENT").equals("production"):false;
            UserService.writeFile(response.getResponseData().getToken(), isProduction);
        }
        System.out.println("Rpa Jar ended!");
    }
}
