package tdp.mt.backend.rpa.jar.service;

import com.fasterxml.jackson.databind.annotation.JsonAppend;
import tdp.mt.backend.rpa.jar.domain.Response;
import tdp.mt.backend.rpa.jar.domain.loginMT.ConvergenciaRequest;
import tdp.mt.backend.rpa.jar.domain.loginMT.ConvergenciaResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import tdp.mt.backend.rpa.jar.util.Constants;
import tdp.mt.backend.rpa.jar.util.Properties;
import tdp.mt.backend.rpa.jar.util.TripleDES;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class UserService {

    private static final Logger logger = LogManager.getLogger();

    public static Response<ConvergenciaResponse> getToken(ConvergenciaRequest req){
        logger.info("Inicio guardarToken Service");
        Response<ConvergenciaResponse> response = new Response<ConvergenciaResponse>();
        ConvergenciaResponse data = new ConvergenciaResponse();
        System.out.println(req.toString());
        try {
            TripleDES cod = new TripleDES("mikey");
            String time = req.getId_transaccion();
            String token=cod.harden(req.getTipdoc()
                    +','+req.getNumero_de_doc()
                    +','+req.getCodatis()
                    +','+time
                    +','+req.getCanal_de_venta()
                    +','+req.getPunto_de_venta()
                    +','+req.getAsesor_nombre()
                    +','+req.getEntidad()
                    +','+req.getCanalequivcampania()
                    +','+req.getHostName()
            );
            logger.info(token);
            data.setToken(token);

            response.setResponseData(data);
            response.setResponseCode(Constants.LOGIN_RESPONSE_CODE_OK);
            logger.info("Exito generarToken Service");
        }catch (Exception e){
            response.setResponseCode(Constants.LOGIN_RESPONSE_CODE_ERROR);
            logger.info("ERROR generarToken Service");
        }
        return response;
    }

    public static String getHostName() {
        InetAddress ip;
        String hostname = null;
        try {
            ip = InetAddress.getLocalHost();
            hostname = ip.getHostName();
            System.out.println("Your current Hostname : " + hostname);

        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        return hostname;
    }

    public static void writeFile(String token, boolean isProduction) {

        String url=isProduction?Constants.MOVISTAR_TOTAL_URL_PRD:Constants.MOVISTAR_TOTAL_URL_DEV;
        token = url + token;

        String path = getPath();

        //File archivo = new File("C:\\Users\\VASSPERU\\Documents\\WAYRAGit\\rpaJARConvergencia\\rpa_jar_convergencia\\out\\archivo.txt");
        File archivo = new File(path);

        BufferedWriter bw;
        try {
            bw = new BufferedWriter(new FileWriter(archivo));
            if(archivo.exists()) {
                // El fichero ya existe
                bw.write(token);
            } else {
                // El fichero no existe y hay que crearlo
                bw.write(token);
            }
            bw.close();
        } catch (Exception e) {
            System.out.println("Error al crear archivo. "+ e);
        }
   }

    private static String getPath() {
        String path = null;
        try {
            path = UserService.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
            if (path.contains(".jar")) {
                path = getPathByLevels(path,1);
            } else {
                path = getPathByLevels(path,3);
            }
            System.out.println(path);

        } catch (Exception e) {
            System.out.println("Error al capturar la ubicacion del jar: " +e.getMessage());
        }
        return path;
    }

    private static String getPathByLevels (String path, int level){
        String rutaFinal = path;
        System.out.println(rutaFinal);
        int i = 0;
        while (i<level) {
            rutaFinal = rutaFinal.substring(0, rutaFinal.lastIndexOf('/'));
            i ++;
        }
        rutaFinal += "/url.txt";
        //System.out.println(rutaFinal);
        return rutaFinal;
    }
}
