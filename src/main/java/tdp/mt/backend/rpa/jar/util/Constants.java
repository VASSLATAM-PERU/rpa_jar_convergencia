package tdp.mt.backend.rpa.jar.util;

public class Constants {

    public static final String LOGIN_RESPONSE_CODE_OK="0";
    public static final String LOGIN_RESPONSE_CODE_ERROR="1";

    //URL
    public static final String MOVISTAR_TOTAL_URL_PRD="https://convergencia-web-prd.mybluemix.net/loginSales?token=";
    public static final String MOVISTAR_TOTAL_URL_DEV="http://convergencia-web.mybluemix.net/loginSales?token=";

    //RUTA
    public static final String FILE_ROUTE=".\\archivo.txt";
    
    //USUARIO ATIS
    public static final String TIPO_DOC = "1";
    public static final String NUMERO_DE_DOC = "71464110";
    public static final String COD_ATIS = "141115";
    public static final String CANAL_DE_VENTA ="CALL CENTER OUT";
    public static final String CANAL_EQUIV_CAMPANIA ="OUT";
    public static final String ENTIDAD = "GSS";
    public static final String PUNTO_DE_VENTA ="GSS OUT CONVERGENTE";
    public static final String ASESOR_NOMBRE="MARIA FE NORIEGA";

    
    

}
