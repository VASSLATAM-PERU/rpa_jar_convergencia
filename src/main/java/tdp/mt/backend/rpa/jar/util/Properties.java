package tdp.mt.backend.rpa.jar.util;

public class Properties {

    private static String PROP_FILE = (System.getenv("JAR_PROPERTIES")!=null?System.getenv("JAR_PROPERTIES"): "dev.properties");

    public static String tipdoc = PropertiesUtil.getPropertyFromFile(PROP_FILE,"seller.tipdoc");
    public static String numero_de_doc = PropertiesUtil.getPropertyFromFile(PROP_FILE,"seller.numero_de_doc");
    public static String codatis = PropertiesUtil.getPropertyFromFile(PROP_FILE,"seller.codatis");
    public static String canal_de_venta = PropertiesUtil.getPropertyFromFile(PROP_FILE,"seller.canal_de_venta");
    public static String punto_de_venta = PropertiesUtil.getPropertyFromFile(PROP_FILE,"seller.punto_de_venta");
    public static String asesor_nombre = PropertiesUtil.getPropertyFromFile(PROP_FILE,"seller.asesor_nombre");
    public static String canalequivcampania = PropertiesUtil.getPropertyFromFile(PROP_FILE,"seller.canalequivcampania");
    public static String entidad = PropertiesUtil.getPropertyFromFile(PROP_FILE,"seller.entidad");

    public static String url = PropertiesUtil.getPropertyFromFile(PROP_FILE,"file.route");

}
